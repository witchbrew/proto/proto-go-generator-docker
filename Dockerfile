FROM golang:1.15.2-alpine3.12

RUN apk update
RUN apk add git
RUN apk add protobuf

RUN go get github.com/golang/protobuf/protoc-gen-go
RUN go get google.golang.org/grpc/cmd/protoc-gen-go-grpc